/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoadelantar;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.LinkedList;

/**
 *
 * @author allanmual
 */
public class Carro {

    private int x;
    private int y;
    private int vel;
    private int velTemp;
    private Color color;
    private boolean adelantar;
    private int ciclosTotales;
    private int ciclosLlevo;
    private int dir;

    public Carro(int x, int y, int vel, Color color, int dir) {
        this.x = x;
        this.y = y;
        this.vel = vel;
        this.color = color;
        this.dir = dir;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillRect(x, y, 40, 25);
    }

    public Rectangle getBounds() {
        return new Rectangle(x, y, 40, 25);
    }

    public void mover(LinkedList<Carro> carros) {

        int res = x;
        if (dir == 1) {
            x -= 3 * vel;
        } else {
            x += 3 * vel;
        }
        Carro temp = voyChocar(carros);
        ciclosTotales = 26;

        x = res;

        if (temp != null && vieneAlguien(carros, ciclosTotales)) {
            x += temp.getVel();
        } else if (temp != null) {
            velTemp = temp.getVel();
            if (!adelantar) {
                adelantar = true;
            }
        } else {
            if (dir == 1) {
                x -= vel;
            } else {
                x += vel;
            }
        }

        if (adelantar) {
            if (ciclosLlevo < 4) {
                y -= 10;
                x += velTemp;
            } else if (ciclosLlevo >= ciclosTotales - 4 && ciclosLlevo < ciclosTotales) {
                y += 10;
                x += vel;
            }
            ciclosLlevo++;

            if (ciclosLlevo > ciclosTotales) {
                adelantar = false;
                velTemp = 0;
                ciclosLlevo = 0;
            } else {
                x += vel;
            }
        } else {
            x += velTemp;
        }

    }

    private Carro voyChocar(LinkedList<Carro> carros) {
        for (Carro carro : carros) {
            if (this != carro && getBounds().intersects(carro.getBounds())) {
                return carro;
            }

        }
        return null;
    }

    public void setVel(int vel) {
        this.vel = vel;
    }

    public int getVel() {
        return vel;
    }

    public int getDir() {
        return dir;
    }

    public int getX() {
        return x;
    }

    private boolean vieneAlguien(LinkedList<Carro> carros, int ciclosTotales) {
        for (Carro carro : carros) {
            if (this != carro && carro.getDir() == 1 && x < carro.getX()+40) {
                int x1 = x + 40 + ciclosTotales * vel;
                int x2 = carro.getX() - ciclosTotales * carro.getVel();
                if (x1 >= x2) {
                    System.out.println(x1);
                    System.out.println(x2);
                    return true;
                }
            }
        }
        return false;
    }

}
