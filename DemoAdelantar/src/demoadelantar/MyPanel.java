/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoadelantar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.LinkedList;
import javax.swing.JPanel;

/**
 *
 * @author allanmual
 */
public class MyPanel extends JPanel implements KeyListener {

    private LinkedList<Carro> carros;

    public MyPanel() {
        init();
        carros = new LinkedList<>();
        carros.add(new Carro(10, 450, 3, Color.CYAN, 3));
        carros.add(new Carro(80, 450, 1, Color.BLUE, 3));
        carros.add(new Carro(260, 450, 1, Color.BLUE, 3));
        carros.add(new Carro(500, 410, 2, Color.BLUE, 1));

    }

    private void init() {
        setBackground(Color.WHITE);
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(600, 560);
    }

    @Override
    public void paint(Graphics gra) {
        super.paint(gra);

        Graphics2D g = (Graphics2D) gra.create();
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 396, 600, 88);
        g.setColor(Color.WHITE);
        g.drawLine(0, 480, 600, 480);
        g.drawLine(0, 400, 600, 400);
        g.setColor(Color.YELLOW);
        g.drawLine(0, 440, 600, 440);

        for (Carro carro : carros) {
            carro.paint(gra);
            carro.mover(carros);
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
