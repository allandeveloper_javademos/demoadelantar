/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package democine;

/**
 *
 * @author AllanDeveloper
 */
public class Cine {

    private Asiento[][] asientos;

    private final String ANSI_RESET = "\u001B[0m";
    private final String ANSI_RED = "\u001B[31m";
    private final String ANSI_GREEN = "\u001B[32m";
    private final int P1 = 3000;
    private final int P2 = 4000;
    private final int P3 = 5000;

    public Cine() {
        asientos = new Asiento[9][9];
        generarAsientos();
    }

    private void generarAsientos() {
        for (int f = 0; f < asientos.length; f++) {
            char letra = (char) (65 + f);
            int precio = f < 3 ? P1 : f < 6 ? P2 : P3;
            for (int c = 0; c < asientos[f].length; c++) {
                asientos[f][c] = new Asiento(letra, (c + 1), precio);
            }
        }
    }

    public String verAsientos() {
        String info = "";
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                info += asientos[f][c].isDisponible() ? ANSI_GREEN : ANSI_RED;
                info += asientos[f][c].getNombre() + " ";
                info += ANSI_RESET;
            }
            info += "\n";
        }
        return info;
    }

    public boolean comprar(String seleccionados) {
        //A1,A2
        String comprados = "";
        String[] espacios = seleccionados.split(",");
        for (int i = 0; i < espacios.length; i++) {
            if (comprarAsiento(espacios[i])) {
                //Informe
                comprados += espacios[i] + ",";
            } else {
                //Liberar
                liberar(comprados);
                return false;
            }
        }
        return true;
    }

    private boolean comprarAsiento(String espacio) {
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (asientos[f][c].getNombre().equals(espacio)) {
                    if (asientos[f][c].isDisponible()) {
                        asientos[f][c].setDisponible(false);
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return false;
    }

    public void liberar(String comprados) {
        String[] espacios = comprados.split(",");
        for (int i = 0; i < espacios.length; i++) {
            liberarAsiento(espacios[i]);
        }
    }

    private void liberarAsiento(String espacio) {
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (espacio.equals(asientos[f][c].getNombre())) {
                    asientos[f][c].setDisponible(true);
                    return;
                }
            }
        }
    }

    public String disponibilidad() {
        String info = "Tipo 1: %d/27\n"
                + "Tipo 2: %d/27\n"
                + "Tipo 3: %d/27\n"
                + " Total: %d/81";
        int p1 = 0;
        int p2 = 0;
        int p3 = 0;
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if (asientos[f][c].getPrecio() == P1 && asientos[f][c].isDisponible()) {
                    p1++;
                } else if (asientos[f][c].getPrecio() == P2 && asientos[f][c].isDisponible()) {
                    p2++;
                } else if (asientos[f][c].isDisponible()) {
                    p3++;
                }
            }
        }

        return String.format(info, p1, p2, p3, (p1 + p2 + p3));
    }

    public int taquilla() {
        int total = 0;
        for (int f = 0; f < asientos.length; f++) {
            for (int c = 0; c < asientos[f].length; c++) {
                if(!asientos[f][c].isDisponible()){
                    total += asientos[f][c].getPrecio();
                }
            }
        }
        return total;
    }

}
