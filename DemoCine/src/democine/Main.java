/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package democine;

/**
 *
 * @author AllanDeveloper
 */
public class Main {

    public static void main(String[] args) {
        Cine ccm = new Cine();
        System.out.println(ccm.verAsientos());
        ccm.comprar("A1,A2,A3");
        System.out.println(ccm.verAsientos());
        ccm.comprar("C1,C2");
        System.out.println(ccm.verAsientos());
        ccm.comprar("I1,I2,I3");
        System.out.println(ccm.verAsientos());
        ccm.comprar("A5,B3,C3");
        System.out.println(ccm.verAsientos());
        ccm.liberar("A1,A2,A3");
        System.out.println(ccm.verAsientos());
        ccm.comprar("A1"); 
        System.out.println(ccm.verAsientos());
        System.out.println(ccm.disponibilidad());
        System.out.println("Taquilla: " + ccm.taquilla());
    }

}
